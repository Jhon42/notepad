CREATE DATABASE IF NOT EXISTS `jj_notepad`;
USE `jj_notepad`;

CREATE TABLE `folders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `old_title` varchar(100),
  `content` text,
  `old_content` text,
  `folder_id` smallint(6) NOT NULL,
  `pin` smallint(6) NOT NULL,
  `creation_date` DATETIME,
  `last_change` DATETIME,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;
