To create the database necessary for the proper functioning of the app,
run the file **dump.sql** using the `mysql -u user -p < dump.sql` command,
replacing *user* with your mysql username.


(Make sure the user has the following permissions **SELECT, INSERT, UPDATE and DELETE**)

And to finish indicate your mysql connection information into the **config.php** file.


Dont forget change your information to connect to the app in the **config.php** file,

by default the login is *LOGIN* and the password is *PASSWORD*.
