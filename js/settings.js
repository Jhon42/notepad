$(document).ready(function() {
  var theme;
  var longPress = 1500;
  var startPress;

  $.post('php/settings.php',
    {setting:"recupInfos"},
    function(data) {
      data = JSON.parse(data);
      theme = data['theme'];
      themeSetting(theme);
      setTimeout(function() {
        themeSet();
      }, 100);
    }
  );
  $('#cross').click(function(){
    var tmp = themeSet();
    $('.menuSettings').animate({marginRight:'-260px'}, 250);
    if (tmp != theme) {
      $.post('php/settings.php',
      {setting:"sendInfos", theme:tmp},
      function() {
        console.log("theme ID:4" + theme);
      });
    }
  });
  $('#safety').on('click', 'p, img', function() {
    $(".folderImgTop").attr('src', 'img/pin.png');
    $(".topTitle").text("Safety PINS");
    display_notes(0);
  });
  $('.newFolder').click(function() {
    $('#menu input').slideDown().focus()
    .on("keyup", function(event) {
      var name = $(this).val().trim();
      if (event.keyCode == 13 && name != '') {
        $.post('php/add-folder.php',
        {name:name},
        function(data){
          $('#folders').html(data);
        });
        display_folders();
        $(this).val('').css({display:'none'});
      }
      if (event.keyCode == 27) {
        $(this).css({display:'none'});
      }
    });
  });
  $('#menu input').focusout(function() {
    $(this).css({display:'none'});
  });
  $('#folders').on('touchstart mousedown', 'p, .folderImg', function() {
    startPress = new Date().getTime();
    var classF = $(this).parent().find('p');
    $(".folderImgTop").attr('src', 'img/folder.png');
    $(".topTitle").text(classF.text());
    $(".topTitle").val(classF.attr('value'));
    display_notes(classF.attr('value'));
  });
  $('#folders').on('touchend mouseup', 'p, .folderImg', function() {
    if (new Date().getTime() >= (startPress + longPress)) {
      $(this).parent().find('.trashFolder').css({display:'block'});
      setTimeout(function() {
        $('.trashFolder').css({display:'none'});
      }, 3000);
    }
  });
  $('#folders').on('mouseup', '.trashFolder', function() {
    var id = $(this).attr('value');
    $.post('php/delete-folder.php',
    {id:id},
    function(){
      console.log("Folder with id (" + id + ") has been deleted");
      display_folders();
      $(".folderImgTop").attr('src', 'img/pin.png');
      $(".topTitle").text("Safety PINS");
      display_notes(0);
    });
  });
  $('#pswdBtn').click(function(){
    var oldPswd = $('#oldPswd').val();
    var newPswd = $('#newPswd').val();
    $.post('php/settings.php',
    {setting:"changePswd", oldPswd:oldPswd, newPswd:newPswd},
    function(data){
      $('.infosPswd').html(data).slideDown();
      $('#oldPswd').val('');
      $('#newPswd').val('');
    });
  });

  $('#reload').click(function(){
    $(this).animate({deg:360}, {duration: 1200,
      step: function(now) {
        $(this).css({transform:'rotate(' + now + 'deg)'});
      }
    });
    $(this).css({deg:0}, {transform:'rotate(0deg)'});
    display_folders();
    if ($('.topTitle').text() == "Safety PINS") {
      display_notes(0);
    } else {
      display_notes($('.topTitle').val());
    }
  });

  $('#logout').click(function(){
    location.replace('index.html');
  });
});

function themeSet() {
  var ret;
  if (document.getElementById("dark").checked == true) {
    ret = 0;
    $('body').css({backgroundColor: '#282C34'});
    $('.menuSettings').css({backgroundColor: '#121212'});
    $('fieldset').css({border: 'solid 1px white'});
    $('#menuSettings h1').css({color: '#266cff'});
    $('#notes p, #textNote').css({color: 'white'});
  }
  if (document.getElementById("light").checked == true) {
    ret = 1;
    $('body').css({backgroundColor: 'white'});
    $('.menuSettings').css({backgroundColor: '#266cff'});
    $('fieldset').css({border: 'solid 1px black'});
    $('#menuSettings h1').css({color: 'white'});
    $('#notes p, #textNote').css({color: 'black'});
  }
  return ret;
};

function themeSetting(theme) {
  var dark = document.getElementById("dark");
  var light = document.getElementById("light");
  if (theme == 0) {
    dark.checked = true;
    light.checked = false;
  }
  if (theme == 1) {
    light.checked = true;
    dark.checked = false;
  }
};
