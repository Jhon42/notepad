$(document).ready(function() {
  $('.hoverSize').hover(
    function(){$(this).children('.emote').animate({
      width: '-=2px',
      height: '-=2px',
      marginTop: '+=1px',
      marginLeft: '+=1px',
      marginRight: '+=1px',
      marginBottom: '+=1px'
    }, 150);},
    function(){$(this).children('.emote').animate({
      width: '+=2px',
      height: '+=2px',
      marginTop: '-=1px',
      marginLeft: '-=1px',
      marginRight: '-=1px',
      marginBottom: '-=1px'
    }, 50);
  });
  $('.hoverThis').hover(
    function(){$(this).animate({
      width: '-=2px',
      height: '-=2px',
      marginTop: '+=1px',
      marginLeft: '+=1px',
      marginRight: '+=1px',
      marginBottom: '+=1px'
    }, 150);},
    function(){$(this).animate({
      width: '+=2px',
      height: '+=2px',
      marginTop: '-=1px',
      marginLeft: '-=1px',
      marginRight: '-=1px',
      marginBottom: '-=1px'
    }, 50);
  });
});
