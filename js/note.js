$(document).ready(function() {
  saveNote(idNote);
  $('#back').click(function() {
    back();
  });
  $('#starPin').click(function() {
    starPin();
  });
});

function back() {
  if ($('#textNote').val() != $('#svNote').val()) {
    console.log("change not saved!");
    $('.check').css({display: 'block', backgroundImage:'url(img/careful.png)'}).animate({opacity:'0'}, 1500);
    setTimeout(function() {
      $('.check').css({display: 'none', opacity:'1.0'});
    }, 1600);
    $('#svNote').val($('#textNote').val());
  } else {
    setTimeout(function() {
      $('.home').css({display: 'block'});
      $('.notes').css({display: 'none'});
      $('.topTitle').css({display:'block', margin:'20px 0 20px 0', fontSize:'24px', top:'10px', left:'65px'});
      $('#titleNote, #textNote').val("");
      $('#starPin').attr('src', 'img/starNone.png');
      $('#folderList').css({display:'none'});
      $('#idNote').text(0);
      $('#svNote').val("");
    }, 200);
    if ($('.topTitle').text() == "Safety PINS") {
      display_notes(0);
    } else {
      display_notes($('.topTitle').val());
    }
  }
};

function saveNote(idNote) {
  $('#saveBtn').click(function() {
    var folderID = setFolderForDB();
    console.log("folderID = " + folderID);
    if (folderID == "" || folderID == "Safety PINS" || folderID === null) {
      console.log("Note not Saved!");
      $('.check').css({display: 'block', backgroundImage:'url(img/pls.png)'}).animate({opacity:'0'}, 1000);
      setTimeout(function() {
        $('.check').css({display: 'none', opacity:'1.0'});
      }, 1100);
    } else {
      var titleNote = $('#titleNote').val();
      var textNote = $('#textNote').val();
      var idNote = $('#idNote').text();
      var pin = $('#starPin').attr('value');
      $.post('php/save-note.php',
      {title:titleNote, content:textNote, folder:folderID, pin:pin, id:idNote},
      function(data) {
        console.log("infos 'saveNote()' : " + data);
        if ($.isNumeric(data) == true) {
          $('#idNote').text(data);
          $('.check').css({display: 'block', backgroundImage:'url(img/check.png)'}).animate({opacity:'0'}, 1000);
          $('#svNote').val($('#textNote').val());
        } else {
          $('.check').css({display: 'block', backgroundImage:'url(img/error.png)'}).animate({opacity:'0'}, 1000);
        }
        setTimeout(function() {
          $('.check').css({display: 'none', opacity:'1.0'});
        }, 1100);
      });
    }
  });
};

function setFolderForDB() {
  var folder_id = $('#folderList').val();
  if (folder_id == "0" || folder_id == "" || folder_id == null) {
    folder_id = $('.topTitle').val();
  }
  return folder_id;
}

function starPin() {
  var starpin = $('#starPin');
  var pin = starpin.attr('value');
  if (pin == 0) {
    starpin.attr('value', 1);
    starpin.attr('src', 'img/star.png');
  }
  if (pin == 1) {
    starpin.attr('value', 0);
    starpin.attr('src', 'img/starNone.png');
  }
};
