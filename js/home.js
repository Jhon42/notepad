$(document).ready(function(){
  var longPress = 1500;
  var startPress;

  $('#idNote').text(0);
  $(".topTitle").text("Safety PINS");
  display_folders();
  display_notes(0);
  $("#plus").click(function() {
    newNote();
  });
  $('#notes').on('touchstart mousedown', 'p, .noteImg, span', function() {
    startPress = new Date().getTime();
  });
  $('#notes').on('touchend mouseup', 'p, .noteImg, span', function() {
    $('.menuSettings').animate({marginRight:'-260px'}, 250);
    if (new Date().getTime() >= (startPress + longPress)) {
      $(this).parent().find('.trash').css({display:'block'});
      $(this).parent().find('.noteImg').css({display:'none'});
      setTimeout(function() {
        $('.trash').css({display:'none'});
        $('.noteImg').css({display:'block'});
      }, 3000);
    }
    else {
      var idNote = $(this).attr('value');
      newNote('#notes');
      $.post('php/note.php',
      {id:idNote},
      function(data){
        data = jQuery.parseJSON(data);
        console.log("note ID : " + data['id']);
        setTimeout(function() {
          $('#titleNote').val(data['title']);
          $('#textNote').val(data['content']);
          $('#svNote').val(data['content']);
          $('.topTitle').val(data['folder_id']);
          $('#textNote').focus();
          $('#idNote').text(data['id']);
          if (data['pin'] == 0) {
            $('#starPin').attr('src', 'img/starNone.png');
          } else {
            $('#starPin').attr('value', 1);
            $('#starPin').attr('src', 'img/star.png');
          }
          $('.topTitle').css({display:'block'});
          $('#folderList').css({display:'none'});
        }, 300);
      });
    }
  });
  $('#notes').on('mouseup', '.trash', function() {
    var id = $(this).attr('value');
    $.post('php/delete-note.php',
    {id:id},
    function() {
      if ($('.topTitle').text() == "Safety PINS") {
        display_notes(0);
      } else {
        display_notes($('.topTitle').val());
      }
      console.log("Note with id (" + id + ") has been deleted");
    });
  });
  $("input:checkbox").on('change', function() {
    $('input[name="' + this.name + '"]').not(this).prop('checked', false);
  });
  $('#menuBtn').click(function(){
    $('#menuSettings').css({display: 'block'});
    $('.menuSettings').animate({marginRight:'0'}, 250);
  });
});

function newNote() {
  setTimeout(function() {
    if ($('.topTitle').text() == "Safety PINS") {
      $.post('php/folders-list.php',
      function(data){
        $('#folderList').html(data);
      });
      $('.topTitle').css({display:'none'});
      $('#folderList').css({display:'block'});
    }
    $('.home').css({display:'none'});
    $('.notes').css({display:'block'});
    $('.topTitle').css({margin:'45px 0 0 65px', fontSize:'16px', top:'5px', left:'20px'});
    $('#titleNote').focus();
  }, 300);
};

function display_folders() {
  $.post('php/folders.php',
  function(data){
    $('#folders').html(data);
  });
};

function display_notes(id) {
  if (id == 0) {
    $.post('php/pins.php',
    function(data){
      $('#notes').html(data);
    });
  } else {
    $.post('php/notes.php',
    {id:id},
    function(data){
      $('#notes').html(data);
    });
  }
};
