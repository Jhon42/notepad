<?php
function conn_db() {
  require("../config/config.php");
  $infos = database_connection();
  $db = new mysqli($infos['servername'], $infos['username'], $infos['password'], $infos['database']);
  if ($db->connect_error) {
    die("(In connect.php) - Connection failed: " . $db->connect_error);
  }
  return $db;
}
?>
