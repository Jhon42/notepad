<?php
require("connect.php");
$db = conn_db();
$folder = $_POST['folder'];

$notes = array();
$recov = $db->query("SELECT * FROM notes");
while($all = $recov->fetch_assoc())
{
  $notes[] = $all;
}
foreach ($notes as $note)
{
  if ($note['pin'] == 1) {
    $date = date('D\.\ M j\, Y \ \a\t\ H:i', strtotime($note['creation_date']));    
    ?><li class="note hoverSize">
      <img class="noteImg emote" value="<?php echo $note['id']?>" src="img/note.png" alt="note">
      <img value="<?php echo $note['id'] ?>" class="trash hoverThis" src="img/trash.png" alt="trash">
      <p value="<?php echo $note['id']?>"><?php echo $note['title'] ?></p>
      <span value="<?php echo $note['id']?>"><?php echo $date ?></span>
    </li><?php
  }
}
mysqli_close($db);
?>
