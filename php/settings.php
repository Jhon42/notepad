<?php
require("connect.php");
$db = conn_db();
$login = $_COOKIE["loginNotepad"];
$setting = $_POST['setting'];

if ($setting == "recupInfos") {
  $res = $db->query("SELECT theme FROM user WHERE login='$login'")->fetch_assoc();
  echo json_encode($res);
}
if ($setting == "sendInfos") {
  $theme = $_POST['theme'];
  $res = $db->query("UPDATE user SET theme='$theme' WHERE login='$login'");
}
if ($setting == "changePswd") {
  $oldPswd = $_POST['oldPswd'];
  $newPswd = $_POST['newPswd'];
  if (!$oldPswd || !$newPswd) {
    echo "<span class='error'>Fill all fields</span>";
  }
  else {
    $res = $db->query("SELECT password FROM user WHERE login='$login'")->fetch_assoc();
    if ($oldPswd == $res['password']) {
      $db->query("UPDATE user SET password='$newPswd' WHERE login='$login'");
      echo "<span class='success'>Password updated!</span>";
    }
    else {
      echo "<span class='error'>Wrong password</span>";
    }
  }
}
mysqli_close($db);
?>
